variable "cluster_name" { 
    type = string
    default = "gitpod-cluster"
}

variable "do_region" {
    type = string
    default = "nyc3"
}

variable "do_k8s_version" {
    type = string
    default = "1.21.5-do.0"
}

variable "droplet_size" {
    type = string
    default = "s-1vcpu-2gb"
}