terraform {
  required_version  = "1.0.11"
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}
resource "digitalocean_kubernetes_cluster" "test_cluster" {
  name    = var.cluster_name
  region  = var.do_region
  version = var.do_k8s_version

  tags = ["gitpod-labs"]

  node_pool {
    name       = "default-pool"
    size       = var.droplet_size
    auto_scale = false
    node_count = 3
    tags       = ["gitpod-labs"]
  }

}